## Getting Started

### Requirements

  * Windows, Linux or Mac OS
  * [Node.js](https://nodejs.org/) v5.0 or newer
  * `npm` v3.3 or newer (are you new to [npm](https://docs.npmjs.com/)?)
  * A text editor or an IDE (whatever floats your boat)

### Directory Layout

Before you start, take a moment to see how the project has been structured:

```
.
├── /docs/                      # Documentation for the project
├── /node_modules/              # Libraries and utils used
├── /src/                       # Contains uncompiled source code
│   ├── /scss/                  # Uncompiled CSS
│   ├── /js/                    # Uncompiled JS
│   ├── /yarn/                  # Yarn dependencies
├── /build/                     # Contains compiled source code
│   ├── /index.html             # Index
│   ├── /css/                   # Compiled CSS
│   ├── /js/                    # Compiled JS
├── LICENSE                     # Project's license
├── .gitignore                  # Self-explanatory
├── .yarnrc                     # Yarn configuration
├── webpack.config.js           # Configuration for front-end bundle
└── package.json                # Project info and list of dependencies used
```

### Quick Start

#### 1. Get the latest version

You'll start off by cloning the Github repo:

```shell
$ git clone -o seed-admin -b master --single-branch \
      https://github.com/basilmeer/seed-admin.git SeedAdmin
$ cd SeedAdmin
```

#### 2. Install the dependencies

Next up, you'll be installing the dependencies so run:

```shell
$ npm install
$ yarn run newinstall
```

In your shell and let it finish.

#### 3. Open `index.html` in /src

Profit. Enjoy.
