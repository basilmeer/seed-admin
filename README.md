## Seed Admin Template

A minimmalistic free-to-use admin dashboard to kickstart projects.

A bit of an ambitious project by yours truly! Hoping it ends well.

## Installation
#### 1. Clone this project or download its .zip file

```sh
$ git clone https://github.com/basilmeer/seed-admin.git
```

#### 2. Be sure to have installed [npm](https://www.npmjs.org/) globally


#### 3. In the command prompt/shell, run the following commands

```sh
$ cd `seed-admin`
$ npm install
$ yarn run newinstall
```

And you should be good to go! Open up index.html in /src to start fiddling with the demo.

## Roadmap/Planned Features (in no specific order yet)

- Try to make it responsive
- Divide the template into components
- Add graphs
- Add skeleton loading
- Add multiple color schemes
- Design a logo
- Add ReactJS


### DISCLAIMER
I haven't touched front-end in a while so this project is my attempt at getting back into the swing of things. It's bound to be riddled with mistakes and bad practices, and if it all annoys you and you'd like to tell me off, then feel free to [hit me up with some advice](mailto:basil@meer.qa) (or if you're on Discord then holler at me and I'll add you).

Thank you for reading and understanding.
